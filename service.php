<?php
ini_set('html_errors', 0);
ini_set('xdebug.overload_var_dump','off');

require_once 'Util.php';


$encoded = $_REQUEST['dto'];

//use this if clientside safeObjToURI is used
$encoded = Util::uriToObject($encoded,Util::BASE64SAFE);


$jsonstring = json_encode($encoded);

//write JSON string to file for validation and debugging purposes
$logfile = fopen('json.txt', "w");
fputs($logfile, $jsonstring);
fclose($logfile);



header('Content-type: application/json');

print $jsonstring;

?>