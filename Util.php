<?php
/**
 * global utitliy helper functions
 * 
 * @author Aaldert van Weelden
 */
class Util{
	
	const TAG = 'Util';
	
	const BASE64 = 'base64';
	const BASE64SAFE = 'base64safe';
	
	private static $instance;
	
	private function __construct(){
		
	}
	
	public static function getInstance() {
		if(!isset(self::$instance)) {
			self::$instance = new Util();
		}
		return self::$instance;
	}
	
    public static function uriToJSONstring($data,$sEncode=false){
            switch($sEncode){
                case self::BASE64:
                    $data=base64_decode($data);
                    $data=utf8_encode($data);
                    break;
                case self::BASE64SAFE:
                    $data=base64_decode($data);
                    $data=self::charcodeToTxt($data);
                    $data=utf8_encode($data);
                    break;
                default :
                    break;
                
            }
            return urldecode($data);
            
    }
    
    public static function uriToObject($data,$sEncode=false){
    	$data =  self::uriToJSONstring($data, $sEncode);
    	return json_decode($data);
    }
	
	public static function charcodeToTxt($value){
		$result='';
	
		$arr = explode('&', $value);
		foreach($arr as $key=>$val){
	
			$sub=explode(';', $val);
			isset($sub[0])?$one=$sub[0]:$one='';
			isset($sub[1])?$two=$sub[1]:$two='';
	
			if( strstr($one,'#') && strlen($one)>1 ){
				$code=intval( str_replace('#','',$sub[0]) );
				if($code>0){
					//var_dump($code)
					$one=chr($code);
				}
			}
			$result.=$one.$two;
		}
		return $result;
	}
}
?>
