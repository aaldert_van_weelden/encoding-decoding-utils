# javascript base64/utf8/charcode utils #

Util classes for encoding and decoding purposes clientside

### Available methods in this Util class###


```
#!javascript

/**
* Encode the provided string to utf8 
* @param String s The string to encode
 */
encode_utf8 : function(s);
		
/**
* Decode the provided string from utf8
* @param String s The string to decode
*/
decode_utf8 : function(s);
		
/**
 * Base64 encode the provided string
 * @param String str  The string to encode
 * @param Boolean safe If TRUE convert all characters to their charcode first
 */
base64encode : function(str,safe);
		
/**
 * Base64 decode the provided string
 * @param String str The string to decode
 * @param Boolean safe If true, the decoded value is converted from charcode to the actual character
 */
base64decode : function(str,safe);
        
/**
 * Convert a text string to a string of correponding charcodes
 */
txtToCharCode : function(txt);
        
/**
 * Convert a string of charcodes to the actual text string
 */
charCodeToTxt : function(code);

 
```