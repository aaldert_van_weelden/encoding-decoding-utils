var Util = (function(){

    /*
     * utf8
     */
	var _utf8 = {
	
		encode : function(sPlaintext){
			
			var plaintext = sPlaintext,
				SAFECHARS = "0123456789" +					// Numeric
							"ABCDEFGHIJKLMNOPQRSTUVWXYZ" +	// Alphabetic
							"abcdefghijklmnopqrstuvwxyz" +
							"-_.!~*'()",					// RFC2396 Mark characters
				HEX = "0123456789ABCDEF",
				encoded = "";
				
			for (var i = 0; i < plaintext.length; i++ ) {
				var ch = plaintext.charAt(i);
				if (ch == " ") {
					encoded += "+";				// x-www-urlencoded, rather than %20
				} 
				else if (SAFECHARS.indexOf(ch) != -1) {
					encoded += ch;
				} 
				else {
					var charCode = ch.charCodeAt(0);
					if (charCode > 255) {
                        /*
						alert( "Unicode Character '" 
								+ ch 
								+ "' cannot be encoded using standard URL encoding.\n" +
								  "(URL encoding only supports 8-bit characters.)\n" +
								  "A space (+) will be substituted." );
                        */
                        
						//encoded += "+";
                        encoded += '|&#'+charCode+';|';
					} 
					else {
						encoded += "%";
						encoded += HEX.charAt((charCode >> 4) & 0xF);
						encoded += HEX.charAt(charCode & 0xF);
					}
				}
			} // for
			return encoded;
		},
	 
		decode : function(sEncoded){
		   
		   var 	encoded = sEncoded,
				HEXCHARS = "0123456789ABCDEFabcdef",
				plaintext = "";
				
		   var i = 0;
		   while (i < encoded.length) {
			   var ch = encoded.charAt(i);
			   if (ch == "+") {
				   plaintext += " ";
				   i++;
			   } 
			   else if (ch == "%") {
					if (i < (encoded.length-2) 
							&& HEXCHARS.indexOf(encoded.charAt(i+1)) != -1 
							&& HEXCHARS.indexOf(encoded.charAt(i+2)) != -1 ) {
						plaintext += unescape( encoded.substr(i,3) );
						i += 3;
					} 
					else {
						//alert( 'Foute escape combinatie bij ...' + encoded.substr(i) );
						plaintext+="%[ERROR]";
						i++;
					}
				} 
				else {
				   plaintext += ch;
				   i++;
				}
			} // while
            
            function convert(value){
                if(_inStr('|&#',value) ){
                    var result='',arr,sub,one,two,code;
                    arr = value.split('|&');
                    for(key in arr){
                    
                        sub=arr[key].split(';|');
                        sub[0]?one=sub[0]:one='';
                        sub[1]?two=sub[1]:two='';
                        
                        if( _inStr('#',one) && one.length>1 ){
                            code=parseInt( sub[0].replace('#','') );
                            if(typeof(code)==='number'){
                                one=String.fromCharCode(code);
                            }
                        }
                        result+=one+two;
                    }
                    return result;
                }
                else return value;
            };
		   plaintext=convert(plaintext);
		   return plaintext;
		}
	};
	
	
	/*
	 * Base64
	 */
    var _txtToCharCode = function(value){
		var result='',arr,code;
		
        arr = value.split('');
        for(key in arr){
            code=arr[key].charCodeAt(0);
            if( code==32||(code>47&&code<58)||(code>67&&code<91)||(code>96&&code<123) ){//only A-Z, a-z, 0-9, space
                result+=arr[key];
            }
            else{
                result+='&#'+arr[key].charCodeAt(0)+';';
            }
        }
		return result;
    };
    
    var _charcodeToTxt = function(value){
		var result='',arr,sub,one,two,code;
		
        arr = value.split('&');
        for(key in arr){
        
            sub=arr[key].split(';');
            sub[0]?one=sub[0]:one='';
            sub[1]?two=sub[1]:two='';
            
            if( _inStr('#',one) && one.length>1 ){
                code=parseInt( sub[0].replace('#','') );
                if(typeof(code)==='number'){
                    one=String.fromCharCode(code);
                }
            }
            result+=one+two;
        }
		return result;
    };

	var _base64 = {
	
		keyStr : "ABCDEFGHIJKLMNOP" +
				 "QRSTUVWXYZabcdef" +
				 "ghijklmnopqrstuv" +
				 "wxyz0123456789+/" +
				 "=",

	   encode : function(input,safe) {
		  var output = "";
          if(input==='' || input===null || typeof(input)==='undefined'){
              return '';
          }
		  var chr1, chr2, chr3 = "";
		  var enc1, enc2, enc3, enc4 = "";
		  var i = 0;
          if(safe){
              input=_txtToCharCode(input);
          }
		  do {
			 chr1 = input.charCodeAt(i++);
			 chr2 = input.charCodeAt(i++);
			 chr3 = input.charCodeAt(i++);

			 enc1 = chr1 >> 2;
			 enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			 enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			 enc4 = chr3 & 63;

			 if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			 } else if (isNaN(chr3)) {
				enc4 = 64;
			 }

			 output = output +
				this.keyStr.charAt(enc1) +
				this.keyStr.charAt(enc2) +
				this.keyStr.charAt(enc3) +
				this.keyStr.charAt(enc4);
			 chr1 = chr2 = chr3 = "";
			 enc1 = enc2 = enc3 = enc4 = "";
		  } while (i < input.length);

		  return output;
	   },

	   decode : function(input, safe) {
			  var output = "";
              if(input==='' || input===null || typeof(input)==='undefined'){
                  return '';
              }
			  var chr1, chr2, chr3 = "";
			  var enc1, enc2, enc3, enc4 = "";
			  var i = 0;

			  // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
			  var base64test = /[^A-Za-z0-9\+\/\=]/g;
			  if (base64test.exec(input)) {
				 alert("There were invalid base64 characters in the input text.\n" +
					   "Valid base64 characters are A-Z, a-z, 0-9, +, /, and =\n" +
					   "Expect errors in decoding.");
			  }
			  input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

			 do 
			 {
				 enc1 = this.keyStr.indexOf(input.charAt(i++));
				 enc2 = this.keyStr.indexOf(input.charAt(i++));
				 enc3 = this.keyStr.indexOf(input.charAt(i++));
				 enc4 = this.keyStr.indexOf(input.charAt(i++));

				 chr1 = (enc1 << 2) | (enc2 >> 4);
				 chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
				 chr3 = ((enc3 & 3) << 6) | enc4;

				 output = output + String.fromCharCode(chr1);

				 if (enc3 != 64) {
					output = output + String.fromCharCode(chr2);
				 }
				 if (enc4 != 64) {
					output = output + String.fromCharCode(chr3);
				 }

				 chr1 = chr2 = chr3 = "";
				 enc1 = enc2 = enc3 = enc4 = "";

			} while (i < input.length);
            
            if(safe){
                output=_charcodeToTxt(output);
            }
			return output;
		}
	};
	
	/**
	 * Public methods
	 */
	
	return {
    
        /**
         * Encode the provided string to utf8 
         * @param String s The string to encode
         */
		utf8encode : function(s){
			return _utf8.encode(s);
		},
		
		/**
		 * Decode the provided string from utf8
		 *  * @param String s The string to decode
		 */
		utf8decode : function(s){
			return _utf8.decode(s);
		},
		
		/**
		 * Base64 encode the provided string
		 * @param String str  The string to encode
		 * @param Boolean safe If TRUE convert all characters to their charcode first
		 */
		base64encode : function(str,safe){
			return _base64.encode(str,safe);
		},
		
		/**
		 * Base64 decode the provided string
		 * @param String str The string to decode
		 * @param Boolean safe If true, the decoded value is converted from charcode to the actual character
		 */
		base64decode : function(str,safe){
			return _base64.decode(str,safe);
		},
        
		/**
		 * Convert a text string to a string of correponding charcodes
		 */
        txtToCharCode : function(txt){
            return _txtToCharCode(txt);
        },
        
        /**
         * Convert a string of charcodes to the actual text string
         */
        charCodeToTxt : function(code){
            return _charcodeToTxt(code);
        },

        /**
         * Convert a javascript plain object to a string suitable to post as URI component to the server
         * @param Object obj  The javascript object to convert
         * @param String mode.  The encoding mode: base64 or base64safe.  
         */
		safeObjToURI : function(obj,mode){
			typeof(mode)==='undefined'||mode==='' ||!mode ? mode='base64':void(0);
			var uri=JSON.stringify(obj);//object to string
			switch(mode){
	            case 'base64':
	                uri=_base64.encode(uri,false);//base64encode
	                break;
	            case 'base64safe':
	                uri= _base64.encode(uri,true);//base64encode with text to charcode conversion
	                break;
	        }
			return uri;
		}
		
	};

})();